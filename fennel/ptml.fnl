(global ptml (or ptml {}))
;https://processing.org/reference/map_.html
;https://stackoverflow.com/questions/3451553/value-remapping
;https://www.desmos.com/calculator/ewnq4hyrbz
(fn ptml.rescale-linear [value imin imax omin omax]
(+ (* (/ (- omax omin) (- imax imin)) (- value imin)) omin))

(fn ptml.rescale-cos [value imin imax omin omax]
(+ (* (- (/ (- omax omin) 2)) (math.cos (/ (* math.pi (- imin value)) (- imin imax)))) (/ (+ omax omin) 2)))

(fn ptml.rescale-arcsin [value imin imax omin omax]
(+ (* (/ (- omax omin) math.pi) (math.asin (* (/ 2 (- imax imin)) (- value (/ (+ imin imax) 2))))) (/ (+ omax omin) 2)))

(fn ptml.rescale-exp [value imin imax omin omax]
(- (+ (^ (+ (- omax omin) 1) (/ (- value imin) (- imax imin))) omin 1) 1))

(fn ptml.rescale-log-min [value imin imax omin omax]
(+ (/ (* (- omax omin) (math.log (+ (- value imin) 1))) (math.log (+ (- imax imin) 1))) omin))

(fn ptml.rescale-log-max [value imin imax omin omax]
(+ (/ (- omin omax) (math.log (+ (- value imin) 1)) (math.log (+ (- imax imin) 1))) omax))

(fn ptml.rescale-para-max [value imin imax omin omax]
(+ (/ (* (- omin omax) (^ (- value imax) 2)) (^ (- imax imin) 2)) omax))

(fn ptml.rescale-para-min [value imin imax omin omax]
(+ (/ (* (- omax omin) (^ (- value imin) 2)) (^ (- imax imin) 2)) omin))

(fn ptml.rescale-ipara [value imin imax omin omax]
(+ (* (/ (- omax omin) (math.sqrt (- imax imin))) (math.sqrt (- value imin))) omin))
