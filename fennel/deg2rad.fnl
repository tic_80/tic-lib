(fn deg2rad [deg]
 (* deg (/ math.pi 180)))
