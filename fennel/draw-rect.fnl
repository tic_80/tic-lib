(fn draw-rect [o w h s a c ?use_fill]
 "Draw a rotating rectangle"
 (local p1 {:x (- o.x (// w 2)) :y (- o.y (// h 2))}) ;top left
 (local p2 {:x (+ o.x (// w 2)) :y (- o.y (// h 2))}) ;top right
 (local p3 {:x (- o.x (// w 2)) :y (+ o.y (// h 2))}) ;bottom left
 (local p4 {:x (+ o.x (// w 2)) :y (+ o.y (// h 2))}) ;bottom right

 (local r1 (rotate-point p1 o a))
 (local r2 (rotate-point p2 o a))
 (local r3 (rotate-point p3 o a))
 (local r4 (rotate-point p4 o a))  
 
 (draw-line r1 r2 c)
 (draw-line r2 r4 c)
 (draw-line r4 r3 c)
 (draw-line r3 r1 c))
