(fn fill [x y to-c]
 "Flood fill a colour"
 (let [on-c (pix x y)]
   (when (not= on-c to-c)
    (fn flood [x y]
     (when (= (pix x y) on-c)
      (pix x y to-c)
      (when (> x 0)
       (flood (- x 1) y))
      (when (< x 239)
       (flood (+ x 1) y))
      (when (> y 0)
       (flood x (- y 1)))
      (when (< y 135)
       (flood x (+ y 1)))))
    (flood x y))))

