(fn draw-line [p1 p2 c]
 "Draw a line with vectors"
 (line p1.x p1.y p2.x p2.y c))
