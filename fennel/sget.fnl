(fn sget [x y]
 "get sprite sheet pixel"
 (local addr (+ 0x4000 (* (+ (// x 8) (* (// y 8) 16)) 32)))
 (peek4 (+ (* addr 2) (% x 8) (* (% y 8) 8))))
