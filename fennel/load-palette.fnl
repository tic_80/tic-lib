(fn load-palette [palette]
 "function uses PALETTES from defined-palettes.fnl"
 (local p (. PALETTES palette))
 (for [i 0 15] 
  (local r (tonumber (string.sub p (+ (* i 6) 1) (+ (* i 6) 2)) 16))
  (local g (tonumber (string.sub p (+ (* i 6) 3) (+ (* i 6) 4)) 16))
  (local b (tonumber (string.sub p (+ (* i 6) 5) (+ (* i 6) 6)) 16))
  (poke (+ 0x3FC0 (* i 3) 0) r)
  (poke (+ 0x3FC0 (* i 3) 1) g)
  (poke (+ 0x3FC0 (* i 3) 2) b)))
