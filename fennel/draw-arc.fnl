;x - center X coordinate of circle
;y - center Y coordinate of circle
;r - the radius of the circle
;c - the circle's color
;st - the radian on circle curcunference to begin drawing at
;en - the radian on circle circumference to end drawing at
(fn draw-arc [x y r c s e hollow fill]
 (var (st en nx ny hx1 hy1 hx2 hy2) 
      (values s e 0 0 0 0 0 0))
 (local fill (or fill false))
 (local hollow (or hollow false))	
 (local tau (* 2 math.pi))
 (local step (/ tau 360))
 (var rs true)
 (if (< st 0) (set st -st))
 (if (< en -1) (set en -en))
 (if (<= en st) (set en (+ en tau)))
 (while (<= st en)
  (set nx (+ x (* r (math.cos st))))
  (set ny (+ y (* r (- (math.sin st)))))
  (when rs
   (set hx1 nx)
   (set hy1 ny)
   (set rs false))   
  (if fill
   (line x y nx ny c)
   (pix nx ny c))
  (set st (+ st (/ step r))))
 (when hollow
  (when (not fill) 
  (set hx2 nx)
  (set hy2 ny)
  (line x y hx1 hy1 c)
  (line x y hx2 hy2 c))))

