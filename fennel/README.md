# fennel

## bitwise.fnl
 * `<<` shift left
 * `>>` shift right

## defined-palettes.fnl
* [SWEETIE16](https://lospec.com/palette-list/sweetie-16)
* [DB16](https://lospec.com/palette-list/dawnbringer-16)
* [PICO8](https://lospec.com/palette-list/pico-8)
* PICO8S 
* [ARNE16](https://lospec.com/palette-list/arne-16)
* [EDG16](https://lospec.com/palette-list/endesga-16)
* [A64](https://lospec.com/palette-list/a64)
* [C64](https://lospec.com/palette-list/commodore64)
* [VIC20](https://lospec.com/palette-list/commodore-vic-20)
* [CGA](https://lospec.com/palette-list/color-graphics-adapter)
* [SLIFE](https://lospec.com/palette-list/still-life)
* [JMP](https://lospec.com/palette-list/jmp-japanese-machine-palette)
* [CGARNE](https://lospec.com/palette-list/cgarne)
* [PSYG](https://lospec.com/palette-list/psygnosia)
* [EROGE](https://lospec.com/palette-list/eroge-copper)
* [EISLAND](https://lospec.com/palette-list/easter-island)
* [RAYLEIGH](https://lospec.com/palette-list/rayleigh)
* [FAMI16](https://lospec.com/palette-list/famicube-16)
* [ARQ16](https://lospec.com/palette-list/arq16)
* [TAFFY16](https://lospec.com/palette-list/taffy-16)
* [MSWINDOWS](https://lospec.com/palette-list/microsoft-windows)
* [IJOY16](https://lospec.com/palette-list/island-joy-16)
* [STEAMLORD](https://lospec.com/palette-list/steam-lords)
* [NA16](https://lospec.com/palette-list/na16)
* [NOTEPAD](https://lospec.com/palette-list/notepad)
* [SNOWL16](https://lospec.com/palette-list/snowl16)
* [FEELSUN16](https://lospec.com/palette-list/feelthesun-16)
* [RAMPY16](https://lospec.com/palette-list/rampy16)
* [NANNER16](https://lospec.com/palette-list/nanner-16)
* [SUMMERPAST](https://lospec.com/palette-list/summers-past-16)
* [ANTIQUITY](https://lospec.com/palette-list/antiquity16)
* [PUNOLITE](https://lospec.com/palette-list/punolite)
* [FANTASY16](https://lospec.com/palette-list/fantasy-16)
* [URBEX16](https://lospec.com/palette-list/urbex-16)
* [COLORBLIND](https://lospec.com/palette-list/colorblind-16)
* [OXYGEN16](https://lospec.com/palette-list/oxygen-16)
* [VERSITLE16](https://lospec.com/palette-list/versitle-16)
* [SHINE16](https://lospec.com/palette-list/shine-16)
* [JONK16](https://lospec.com/palette-list/jonk-16)
* [MELODY16](https://lospec.com/palette-list/melody-16)
* [SPOCKETBOY](https://lospec.com/palette-list/super-pocket-boy)
* [BLOOM16](https://lospec.com/palette-list/bloom-16)
* [FOREST16](https://lospec.com/palette-list/forest-16)
* [MACINTOSH2](https://lospec.com/palette-list/macintosh-ii)
* [CHIP16](https://lospec.com/palette-list/chip16)
* [PYXEL](https://lospec.com/palette-list/pyxel)
* [POLLEN8](https://lospec.com/palette-list/pollen8)
* [DREAMS8](https://lospec.com/palette-list/dreamscape8)
* [BUBBLEGUM](https://lospec.com/palette-list/bubblegum-16)
* [JUSTPARCH](https://lospec.com/palette-list/justparchment8)
* [NOPAL12](https://lospec.com/palette-list/nopal-12)
* [DEMICHROME](https://lospec.com/palette-list/2bit-demichrome)
* [LOSTCENT16](https://lospec.com/palette-list/2bit-demichrome)
* [LAVAGB](https://lospec.com/palette-list/lava-gb)
* [MIDNIBLAZE](https://lospec.com/palette-list/midnight-ablaze)
* [INK](https://lospec.com/palette-list/ink)
* [GALAXYFLAM](https://lospec.com/palette-list/galaxy-flame)
* [WINTERWNDR](https://lospec.com/palette-list/winter-wonderland)
* [VMILKSHAKE](https://lospec.com/palette-list/vanilla-milkshake)
* [GRAPESODA](https://lospec.com/palette-list/grape-soda)
* [INKPINK](https://lospec.com/palette-list/inkpink)
* [PAPER8](https://lospec.com/palette-list/paper-8)
* [PASTEL15](https://lospec.com/palette-list/oh-hell-pastel-15)
* [CITRINK](https://lospec.com/palette-list/citrink)
* [PASTELQT](https://lospec.com/palette-list/pastel-qt)
* [KIRBY15](https://lospec.com/palette-list/kirby-15)
* [CASTPIXEL](https://lospec.com/palette-list/castpixel-16)
* [ZXSPECTRUM](https://lospec.com/palette-list/zx-spectrum-adjusted)
* [DARKSEED16](https://lospec.com/palette-list/darkseed-16)
* [DREAMHAZE8](https://lospec.com/palette-list/dream-haze-8)
* [SYZ15](https://lospec.com/palette-list/syz15)
* [CURIOS](https://lospec.com/palette-list/curiosities)
* [PEACHPOP](https://lospec.com/palette-list/peachy-pop-16)
* [15PDX](https://lospec.com/palette-list/15p-dx)
* [COFFEE9](https://lospec.com/palette-list/coffee-9)
* [GAMEBOY](https://lospec.com/palette-list/nintendo-gameboy-arne)

## draw-line.fnl
Draw a line using vectors

## draw-rectangle.fnl
Draw a rotating rectangle

## flood-fill.fnl
Flood fill a colour ad point [x y]

## load-palette.fnl
This function ingests the PALETTE variable from defined-palettes and uses your selection to change the palette

## rotate-point
Rotate a vector around an origin

## rspr.fnl
Custom sprite function that allows for scale and rotate using textri to draw it to screen

## sset.fnl
Set a spritesheet pixel

## sget.fnl
Get a spritesheet pixel