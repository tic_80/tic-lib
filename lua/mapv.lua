function mapv(value,start1,stop1,start2,stop2)
 --https://processing.org/reference/map_.html
 --https://stackoverflow.com/questions/3451553/value-remapping
	return start2+(stop2-start2)*((value-start1)/(stop1-start1))
end
