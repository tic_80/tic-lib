function sset(x, y, c)
 local addr = (16384 + (((x // 8) + ((y // 8) * 16)) * 32))
 return poke4(((addr * 2) + (x % 8) + ((y % 8) * 8)), c)
end
