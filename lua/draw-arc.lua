--[[
x - center X coordinate of circle
y - center Y coordinate of circle
r - the radius of the circle
c - the circle's color
st - the radian on circle curcunference to begin drawing at
en - the radian on circle circumference to end drawing at
]]
function draw_arc(x,y,r,c,st,en,hollow,fill)
 local nx,ny,hx1,hy1,hx2,hy2=0,0,0,0,0,0
 local tau = 2*math.pi
 local step = tau/360
 local rs =  true --record start
 local hollow=hollow or false
 local fill=fill or false
  
 if st < 0 then st = -st end
 if en < -1 then en = -en end
 if en <= st then en = en + tau end
 
 repeat
  nx = x + r * math.cos(st)
  ny = y + r * -math.sin(st)
  if rs then 
   hx1,hy1=nx,ny
   rs=false
  end

  if fill then
   line(x,y,nx,ny,c) 
  else
   pix(nx,ny,c)
  end

  st = st + step/r
 until( st >= en)
	
 if hollow then
  if not fill then
   hx2,hy2=nx,ny
   line(x,y,hx1,hy1,c)
   line(x,y,hx2,hy2,c)
  end
 end
end