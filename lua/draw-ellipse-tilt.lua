function draw_ellipse_tilt(x,y,a,b,r,c)
 local tau = 2*math.pi
 local step = tau/360
 local sin = math.sin
 local cos = math.cos
 local u,v=0,0
	for k=0,tau+step,step do
		i=a * cos(k) * cos(r) + b * sin(k) * sin(r)
		j=-a * cos(k) * sin(r) + b * sin(k) * cos(r)
		i = i + x
		j = -j + y
		if k ~= 0 then
		 --line(u,v,i,j,c)
			pix(i,j,c)
		else
			pix(i,j,c)
			u=i
			v=j
		end				 
	end
end