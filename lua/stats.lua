FPS = {}
function FPS:new(o)
 o = o or {}
 setmetatable(o, self)
 self.__index = self
 self.value = 0
 self.frames = 0
 self.lastTime = 0
 return FPS
end

function FPS:getValue()
 if (time() - self.lastTime <= 1000) then
  self.frames = self.frames + 1
 else
  self.value = self.frames
  self.frames = 0
  self.lastTime = time()
 end
 return self.value
end

function stats()
 if debug then
   rect(0, 0, screen_width,5, 15)
   fpsv="FPS: " .. fps:getValue()
   print(fpsv, (screen_width / 2)-(#fpsv*6/2), 0, 11, false, 1, false)
 end
end
