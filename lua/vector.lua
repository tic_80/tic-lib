function nop(...) return ... end

--------------------------------
-- simple object type
local obj = {init = nop}
obj.__index = obj

function obj:__call(...)
    local o = setmetatable({}, self)
    return o, o:init(...)
end

-- subclassing
function obj:extend(proto)
 proto = proto or {}
 proto.__call, proto.__index =
 self.__call, proto
 return setmetatable(proto, self)
end

local Vector = obj:extend{}

function Vector:init(x, y)
 self.x, self.y =x or 0, y or x or 0
end

function Vector:__add(v)
 return Vector(self.x + v.x, self.y + v.y)
end

function Vector:__sub(v)
 return Vector(self.x - v.x, self.y - v.y)
end
function Vector:__mul(n)
 return Vector(self.x * n, self.y * n)
end
function Vector:elemx(v)
 return Vector(self.x * v.x, self.y * v.y)
end
function Vector:unpack()
 return table.unpack({self.y, self.y})
end

function Vector:iadd(v)
 self.x = self.x + v.x
 self.y = self.y + v.y
end
